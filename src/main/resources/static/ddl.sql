create sequence item_sequence
;

create sequence tag_sequence
;

create sequence context_sequence
;

create sequence user_sequence
;

create sequence role_sequence
;

create table item
(
  id bigint not null
    constraint bucket_pkey
    primary key,
  text varchar not null,
  creation_date timestamp not null,
  user_id bigint not null
)
;

create table bucket
(
  id bigint not null
)
;

create table calendar
(
  id bigint not null,
  eval_date time not null,
  parent_id bigint
)
;

create table context
(
  id bigint not null
    constraint context_pkey
    primary key,
  text varchar not null
)
;

create table def_item
(
  id bigint not null,
  notification_date time not null
)
;

create table info
(
  id bigint not null,
  parent_id bigint
)
;

create table project
(
  id bigint not null,
  start_date time,
  end_date time,
  acceptance_criteria varchar,
  "desc" varchar,
  goal_amount bigint,
  current_amount bigint,
  parent_id bigint
)
;

create table tag
(
  id bigint not null
    constraint tag_pkey
    primary key,
  text varchar not null
)
;

create table todo
(
  id bigint not null,
  parent_id bigint,
  "order" bigint
)
;

create table item_tag
(
  item_id bigint not null,
  tag_id bigint not null
)
;

create table item_context
(
  item_id bigint not null,
  context_id bigint not null
)
;

create table role
(
  id bigint not null
    constraint role_pkey
    primary key,
  name varchar(10) not null
)
;

create table "user"
(
  id bigint not null
    constraint user_pkey
    primary key,
  login varchar(20) not null,
  password varchar(50) not null
)
;

