package com.sgrow.platform.repository;

import com.sgrow.platform.entity.TodoItem;

import java.util.List;

public interface TodoRepository extends ItemRepository<TodoItem> {
    List<TodoItem> findByParentId(long parentId);

    List<TodoItem> findByUserIdAndTagId(long userId, long tagId);

    List<TodoItem> findByUserIdAndContextId(long userId, long contextId);
}
