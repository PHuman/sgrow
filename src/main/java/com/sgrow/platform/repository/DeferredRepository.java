package com.sgrow.platform.repository;

import com.sgrow.platform.entity.DeferredItem;

import java.time.LocalDateTime;
import java.util.List;

public interface DeferredRepository extends ItemRepository<DeferredItem> {

    List<DeferredItem> findByUserIdAndNotificationDate(long userId, LocalDateTime notificationDate);
}
