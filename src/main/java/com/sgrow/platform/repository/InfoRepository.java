package com.sgrow.platform.repository;

import com.sgrow.platform.entity.InfoItem;

import java.util.List;

public interface InfoRepository extends ItemRepository<InfoItem> {

    List<InfoItem> findByParentId(long parentId);

    List<InfoItem> findByUserIdAndTagId(long userId, long tagId);

    List<InfoItem> findByUserIdAndContextId(long userId, long contextId);

}
