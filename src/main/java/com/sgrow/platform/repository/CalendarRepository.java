package com.sgrow.platform.repository;

import com.sgrow.platform.entity.CalendarItem;

import java.time.LocalDateTime;
import java.util.List;

public interface CalendarRepository extends ItemRepository<CalendarItem> {

    List<CalendarItem> findByUserIdAndEvaluationDateTime(Long userId, LocalDateTime date);
}
