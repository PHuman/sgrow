package com.sgrow.platform.repository;

import com.sgrow.platform.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ItemRepository<T extends Item> extends JpaRepository<T, Long> {

    List<T> findByUserId(long userId);
}
