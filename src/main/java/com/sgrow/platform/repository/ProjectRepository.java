package com.sgrow.platform.repository;

import com.sgrow.platform.entity.ProjectItem;

import java.util.List;

public interface ProjectRepository extends ItemRepository<ProjectItem> {

    List<ProjectItem> findByParentId(long parentId);

    List<ProjectItem> findByUserIdAndTagId(long userId, long tagId);

    List<ProjectItem> findByUserIdAndContextId(long userId, long contextId);
}
