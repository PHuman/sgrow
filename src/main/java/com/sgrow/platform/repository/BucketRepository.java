package com.sgrow.platform.repository;

import com.sgrow.platform.entity.BucketItem;

public interface BucketRepository extends ItemRepository<BucketItem> {
}
