package com.sgrow.platform;

import com.sgrow.platform.rest.impl.BucketRestServiceImpl;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(BucketRestServiceImpl.class);
    }
}
