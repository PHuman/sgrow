package com.sgrow.platform.rest.impl;

import com.sgrow.platform.entity.BucketItem;
import com.sgrow.platform.repository.BucketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.time.LocalDateTime;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/bucket")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Service
public class BucketRestServiceImpl {

    private final BucketRepository repository;

    @Autowired
    public BucketRestServiceImpl(BucketRepository repository) {
        this.repository = repository;
    }

    @POST
    public Response add(BucketItem item) {
        item.setCreationDate(LocalDateTime.now());
        return Response.ok(repository.save(item)).build();
    }

    @POST
    @Path("/multi_read")
    public Response findByUser(ItemContext context) {
        return Response.ok(repository.findByUserId(context.getUserId())).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        repository.delete(id);
        return Response.noContent().build();
    }
}
