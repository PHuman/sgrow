package com.sgrow.platform.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

import static javax.persistence.InheritanceType.JOINED;

@Data
@Entity
@Table(name = "item")
@Inheritance(strategy = JOINED)
public class Item implements Serializable {
    @Id
    @SequenceGenerator(name = "item_sequence", sequenceName = "item_sequence", allocationSize = 1)
    @GeneratedValue(generator = "item_sequence")
    private long id;

    private String text;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "user_id")
    private long userId;
}
