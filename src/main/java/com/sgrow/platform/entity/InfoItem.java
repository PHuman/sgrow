package com.sgrow.platform.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "info")
public class InfoItem extends Item {

    // TODO: 13.08.2017 use id instead
    @JoinColumn(name = "parent_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProjectItem parent;

    @ManyToMany
    @JoinTable(name = "item_tag", joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tag;

    @ManyToMany
    @JoinTable(name = "item_context", joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "context_id"))
    private List<Context> context;
}
