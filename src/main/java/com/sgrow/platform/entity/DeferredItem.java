package com.sgrow.platform.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "def_item")
public class DeferredItem extends Item {

    @Column(name = "notification_date")
    private LocalDateTime notificationDate;
}
