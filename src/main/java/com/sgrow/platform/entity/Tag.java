package com.sgrow.platform.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tag")
public class Tag {
    @Id
    @SequenceGenerator(name = "tag_sequence", sequenceName = "tag_sequence", allocationSize = 1)
    @GeneratedValue(generator = "tag_sequence")
    private long id;

    @Column(unique = true)
    private String text;
}
