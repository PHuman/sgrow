package com.sgrow.platform.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "project")
public class ProjectItem extends Item {

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate endDate;

    @Column(name = "acceptance_criteria")
    private String acceptanceCriteria;

    @Column(name = "desc")
    private String description;

    @Column(name = "goal_amount")
    private long goalAmount;

    @Column(name = "current_amount")
    private long currentAmount;

    @JoinColumn(name = "parent_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProjectItem parent;

    @OneToMany(mappedBy = "parent")
    private List<TodoItem> todoItems;

    @OneToMany(mappedBy = "parent")
    private List<CalendarItem> calendarItems;

    @OneToMany(mappedBy = "parent")
    private List<InfoItem> infoItems;

    @OneToMany(mappedBy = "parent")
    private List<ProjectItem> projectItems;

    @ManyToMany
    @JoinTable(name = "item_tag", joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private List<Tag> tag;

    @ManyToMany
    @JoinTable(name = "item_context", joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "context_id"))
    private List<Context> context;
}
