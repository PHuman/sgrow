package com.sgrow.platform.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "calendar")
public class CalendarItem extends Item {


    // TODO: 13.08.2017  use only id
    @JoinColumn(name = "parent_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProjectItem parent;

    @Column(name = "eval_date")
    private LocalDateTime evaluationDateTime;

    // TODO: 13.08.2017
    //private Frequency frequency;


}
