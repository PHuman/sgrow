package com.sgrow.platform.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "context")
public class Context {

    @Id
    @SequenceGenerator(name = "context_sequence", sequenceName = "context_sequence", allocationSize = 1)
    @GeneratedValue(generator = "context_sequence")
    private long id;

    @Column(unique = true)
    private String text;
}
